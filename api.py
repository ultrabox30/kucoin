#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo
import requests
import json
import re
import threading
import segno
import io
import os
import uuid
from io import BytesIO
from flask import Response 
from bson import json_util
from twocaptcha import TwoCaptcha
import time

from flask_cors import CORS, cross_origin

from pymongo import MongoClient
import random
from random import randint
import base64
from urllib.request import urlopen

from requests.auth import HTTPProxyAuth

import time
from datetime import datetime

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
import hashlib

from flask import (
    Flask,
    jsonify,
    request,
    send_from_directory
)

client = MongoClient('mongodb://localhost:27017/kucoin')


class KUCOIN:
    
    def __init__(self):
        self.proxy = False
        self.id = "asdas"
        self.session = requests.session()
        self.captchaApiKey = 'db887fba6c7b7dc5140e13f720e5af93'
        self.setProxy()
        self.db = client.kucoin
        


    def criptografaSenha(self,senha,contador=3):
        n = "_kucoin_"
        
        newPass1 = n + senha + n
        newPass1 = newPass1.encode('utf-8')
        newPass1Hash =  hashlib.md5(newPass1)
        newPass1Hash = newPass1Hash.hexdigest()
        contador = contador - 1

        if(contador > 0):
            return self.criptografaSenha(newPass1Hash,contador)
        else:
            return newPass1Hash


    def getGeetestResolvido(self,tipo):
        geetestResolvido = self.db.geetest.find_one({'isUsado':False, 'tipo' : tipo})


        print(geetestResolvido,tipo)

        if geetestResolvido:
            objToUpdate = {
                "$set": {
                    "isUsado" : True,
                    "updatedAt" : datetime.utcnow(),
                }
            }

            geetestId = self.db.geetest.update_one({"_id":geetestResolvido['_id']}, objToUpdate)

            del geetestResolvido['_id']

            return geetestResolvido
        else:
            return ''


    def validarGeetestNovo(self,geetest,tipo='EMAIL'):
        headers = self.getHeaders(geetest['dispositivo'])
    

        data = {
            "bizType" : tipo+"_LOGIN",
            "captchaType" : "GEETEST",
            "secret" : 'e39ef2c1b251d08e40c23642e879eaa8',
            "response" : json.dumps({
                "geetest_challenge" : geetest["geetest_challenge"],
                "geetest_seccode" : geetest["geetest_seccode"],
                "geetest_validate" : geetest["geetest_validate"]
            })
        }

        response = requests.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/captcha-validation', headers=headers, data=data, verify=False)

        return response


    def loginNovo(self,login,senha,tipo):
        geetest = False

        while(geetest == False):
            print('SEM CAPTCHA')
            getGeetestResolvido = self.getGeetestResolvido(tipo)

           
            if getGeetestResolvido:
                geetest = getGeetestResolvido
            time.sleep(1)

        

        if geetest:
            
            resValidaGeetestNovo = self.validarGeetestNovo(geetest)
            #jsonResValidaGeetestNovo = json.loads(resValidaGeetestNovo.text)

            print(resValidaGeetestNovo.text)

            #validou o geetest
            if '"success":true' in resValidaGeetestNovo.text:

                headers = self.getHeaders(geetest['dispositivo'])

                senha = self.criptografaSenha(senha)

                data = 'loginWay='+tipo+'&userName='+login+'&password='+senha+'&anonymousId=6e64a62faa5dc69a'

                if (self.proxy):
                    response = self.session.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/login', headers=headers, data=data, verify=False, proxies = self.proxy)
                else:
                    response = self.session.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/login', headers=headers, data=data, verify=False)

                #salvo no banco de dados
                if '{"success":true' in response.text:

                    loginDB = self.db.login
                    obj = {
                        "login" : login,
                        "senha" : senha,
                        "tipo" : tipo,
                        "dispositivo": geetest['dispositivo'],
                        "saldo" : 0,
                        "createdAt" : datetime.utcnow(),
                        "updatedAt" : datetime.utcnow(),
                        "respostaLogin" : response.text
                    }

                    loginDB.insert_one(obj).inserted_id



                return response.text
            else:
                if '{"success":false' in  resValidaGeetestNovo.text:
                    return self.loginNovo(login,senha,tipo)
        
        return 'OCORREU UM ERRO'



    def getHeaders(self,deviceId='24a5ca1e583f1320cab3e76e8add386c7'):
        headers = {
            'Host': 'appapi-v2.xcoinsystem.com',
            'x-app-version': '3.51.0',
            'x-device': 'G011A',
            'x-device-no': deviceId,
            'x-device-info': 'google/google/G011A:7.1.2/20171130.376229:user/release-keys',
            'x-app_id': 'com.kubi.kucoin',
            'x-system': 'android',
            'x-is-cn': 'false',
            'x-system-version': '7.1.2',
            'lang': 'pt_PT',
            'user-agent': 'android/3.51.0',
            'x-app-token': '',
            'token': 'BvDVJyYElAr4ZeytH55NT9V3od6sH0_saMUOxNA3XApiTVBTCfTF86EYPVQ3lgygN5eFPDWzp-xMbJC-3qyei9veuzPQ3pvUNtrInPao2NA=',
            'content-type': 'application/x-www-form-urlencoded',
        }
        return headers


    def getGeetest(self,tipo):
        headers = {
            'Host': 'appapi-v2.xcoinsystem.com',
            'x-app-version': '3.51.0',
            'x-device': 'DMCJO',
            'x-device-no': '200dfc122325c2340a833a0e0332226b5e',
            'x-device-info': 'Samsung/19112/191:14/D1IX.938274.516/DMCJOLOBRW1VJ:user/release-keys',
            'x-app_id': 'com.kubi.kucoin',
            'x-system': 'android',
            'x-is-cn': 'false',
            'x-system-version': '7.1.2',
            'lang': 'pt_PT',
            'user-agent': 'android/3.51.0',
            'x-app-token': '',
        }

        params = (
            ('bizType', tipo+'_LOGIN'),
            ('gtType', '2'),
        )

        if self.proxy:
            response = self.session.get('https://appapi-v2.xcoinsystem.com/app/v1/auth/captcha-init', headers=headers, params=params, verify=False, proxies = self.proxy)
        else:    
            response = self.session.get('https://appapi-v2.xcoinsystem.com/app/v1/auth/captcha-init', headers=headers, params=params, verify=False)

        return response

    def validarGeetest(self,response,secret,tipo):
        headers = self.getHeaders()
        
        response = json.loads(response)

        data = {
            "bizType" : tipo+"_LOGIN",
            "captchaType" : "GEETEST",
            "secret" : secret,
            "response" : json.dumps({
                "geetest_challenge" : response["geetest_challenge"],
                "geetest_seccode" : response["geetest_seccode"],
                "geetest_validate" : response["geetest_validate"]
            })
        }

        response = requests.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/captcha-validation', headers=headers, data=data, verify=False)

        return response


    def resolveGeetestApi(self,desafio):

        solver = TwoCaptcha('db887fba6c7b7dc5140e13f720e5af93')

        try:
            result = solver.geetest(gt='e39ef2c1b251d08e40c23642e879eaa8',
                                    challenge=desafio,
                                    url='https://www.kucoin.com/')

            return {
                "result" : result,
                "gt" : 'e39ef2c1b251d08e40c23642e879eaa8'
            }
        except Exception as e:
            print(e)
            return self.resolveGeetestApi(desafio)

    def resolveGeetest(self,tipo):

        geetest = self.getGeetest(tipo)
        geetestObj = json.loads(geetest.text)

        print(geetest.text)

        if geetestObj['success']:

            solver = TwoCaptcha('db887fba6c7b7dc5140e13f720e5af93')

            try:
                result = solver.geetest(gt=geetestObj['data']['gt'],
                                        challenge=geetestObj['data']['challenge'],
                                        url='https://www.kucoin.com/')

                return {
                    "result" : result,
                    "gt" : geetestObj['data']['gt']
                }
            except Exception as e:
                print(e)
                return self.resolveGeetest(tipo)

    def validarLogin(self,token):
        headers = self.getHeaders()

        data = 'code=&token='+token+'&validationType=&validationWay=loginToken'

        response = requests.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/login-validation', headers=headers, data=data,verify=False)      

        return response
            

    def login(self,login,senha,tipo):

        geetestResolvido = self.resolveGeetest(tipo)
        resValidarGeetest = self.validarGeetest(geetestResolvido['result']['code'],geetestResolvido['gt'],tipo)
        resValidarGeetest = json.loads(resValidarGeetest.text)

        if resValidarGeetest['success']:

            headers = self.getHeaders()

            senha = self.criptografaSenha(senha)

            data = 'loginWay='+tipo+'&userName='+login+'&password='+senha+'&anonymousId=6e64a62faa5dc69a'

            response = self.session.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/login', headers=headers, data=data, verify=False)

            return response.text


    def validaToken(self,loginToken):


        data = '{"channel":"MY_SMS","loginToken":"6974ef4a-97b5-4ada-b539-3660321fb1bb","receiver":"","validationBiz":"LOGIN"}'

        response = requests.post('https://appapi-v2.xcoinsystem.com/app/v1/auth/validation-code', headers=headers, data=data)

    def setProxy(self): 
        #http_proxy = "http://FYKpJTFN6I-cc-br-sid-50062111:7p6jKC6W@gw.proxy.rainproxy.io:5959"

        http_proxy = "http://01mskr5j4R-cc-any:EQZinABI@gw.proxy.rainproxy.io:5959"

        
        
        proxyDict = { 
          "http"  : http_proxy, 
          "https" : http_proxy,
        }
        
        self.proxy = proxyDict



# Create the application instance
app = Flask(__name__)

# Create a URL route in our application for "/"
@app.route('/')
def home():

    var = "123123"
    ku = KUCOIN()
    senha = ku.criptografaSenha('12312312')

    return jsonify(['TO ONLINE',senha])

# TIPO = PHONE | EMAIL 
@app.route('/login/<login>/<senha>/<tipo>')
def login(login,senha,tipo):

    ku = KUCOIN()
    resLogin = ku.login(login,senha,tipo)

    resLoginObj = json.loads(resLogin)

    if resLoginObj['success']:
        res = ku.validarLogin(resLoginObj['data']['loginToken'])
        return res.text

    return resLogin


@app.route('/validaToken/<login>')
def validaToken(login):

    ku = KUCOIN()
    resLogin = ku.validaToken()


    return resLogin


@app.route('/criptografa-senha/<senha>')
def criptografaSenha(senha):

    ku = KUCOIN()
    resLogin = ku.criptografaSenha(senha)


    return resLogin


@app.route('/resolve-geetest/<desafio>')
def resolveGeetest(desafio):

    ku = KUCOIN()
    resLogin = ku.resolveGeetest('EMAIL')


    return resLogin

@app.route('/get-geetest-resolvido')
def getGeetestResolvido():

    ku = KUCOIN()
    resLogin = ku.getGeetestResolvido()
     

    return resLogin


@app.route('/login-novo',methods=['POST'])
@cross_origin(supports_credentials=True)
def loginNovo():

    resLogin = ''


    print(request.form)

    if request.method == 'POST':
        login = request.form.get('login')
        senha = request.form.get('senha')
        tipo = request.form.get('tipo')  


        print(login,senha,tipo)

        ku = KUCOIN()
        resLogin = ku.loginNovo(login,senha,tipo)
     

    return resLogin


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0',port=3010,debug=True)

