import pymongo
import requests
import json
import re
import threading
import string
from bs4 import BeautifulSoup
from pymongo import MongoClient
import random
from random import randint

from requests.auth import HTTPProxyAuth

from datetime import datetime
from twocaptcha import TwoCaptcha



import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

client = MongoClient('mongodb://localhost:27017/kucoin')


class CAIXATEM:

    def __init__(self):
        self.proxy = False
        self.setProxy()
        self.session  = requests.session()
        self.db = client.kucoin

    def setProxy(self): 
        http_proxy = "http://01mskr5j4R-cc-any:EQZinABI@gw.proxy.rainproxy.io:5959"

        proxyDict = { 
          "http"  : http_proxy, 
          "https" : http_proxy,
        }
        
        self.proxy = proxyDict



  
    def geraIdDispositivo(self):
        gerador = '{}dfc{}c2340a833a0e{}b{}e' \
            .format(random.randint(100,999),
                    random.randint(100000,999999),
                    random.randint(1000000,9999999),
                    random.randint(0,9),
                    )

        return gerador


    def getGeetest(self,tipo,dispositivo):

        headers = {
            'Host': 'appapi-v2.xcoinsystem.com',
            'x-app-version': '3.51.0',
            'x-device': 'DMCJO',
            'x-device-no': dispositivo,
            'x-device-info': 'Samsung/19112/191:14/D1IX.938274.516/DMCJOLOBRW1VJ:user/release-keys',
            'x-app_id': 'com.kubi.kucoin',
            'x-system': 'android',
            'x-is-cn': 'false',
            'x-system-version': '7.1.2',
            'lang': 'pt_PT',
            'user-agent': 'android/3.51.0',
            'x-app-token': '',
        }

        params = (
            ('bizType', tipo+'_LOGIN'),
            ('gtType', '2'),
        )

        if self.proxy:
            response = self.session.get('https://appapi-v2.xcoinsystem.com/app/v1/auth/captcha-init', headers=headers, params=params, verify=False, proxies = self.proxy)
        else:    
            response = self.session.get('https://appapi-v2.xcoinsystem.com/app/v1/auth/captcha-init', headers=headers, params=params, verify=False)

        return response

    def resolveGeetest(self,tipo):

        dispositivo = self.geraIdDispositivo()

        geetest = self.getGeetest(tipo,dispositivo)
        geetestObj = json.loads(geetest.text)

        if geetestObj['success']:

            solver = TwoCaptcha('db887fba6c7b7dc5140e13f720e5af93')

            try:
                result = solver.geetest(gt=geetestObj['data']['gt'],
                                        challenge=geetestObj['data']['challenge'],
                                        url='https://www.kucoin.com/')

              

                

                result = json.loads(result['code'])

                print(result)

                geetest = self.db.geetest
                obj = {
                    "dispositivo" : dispositivo,
                    "geetest_challenge" : result['geetest_challenge'],
                    "geetest_validate" : result['geetest_validate'],
                    "geetest_seccode" : result['geetest_seccode'],
                    "createdAt" : datetime.utcnow(),
                    "updatedAt" : datetime.utcnow(),
                    "tipo" : tipo,
                    "isUsado" : False
                }

                idDeetest = geetest.insert_one(obj).inserted_id
            
                return {
                    "result" : result,
                    "gt" : geetestObj['data']['gt']
                }
            except Exception as e:
                print(e)
                return self.resolveGeetest(tipo)    
        
       



def thr(i,limite):
    
    #print(cpf)
    try:
    
        caixaTem = CAIXATEM()
        r = caixaTem.resolveGeetest('EMAIL')

        caixaTem = CAIXATEM()
        r = caixaTem.resolveGeetest('PHONE')

        print(r)

        #checkPessoa(cpf,listaproxy)
        limite.release()
    except Exception as e:
        limite.release()
        print(e)
        pass


def load(txt):
        abc = []
        for r in open(txt):
            abc.append(r.strip())
        return abc


def init():
    
    try:
      
        numeroThr = 3
        
        
        thread = numeroThr
        limit = threading.BoundedSemaphore(value=thread)
        threads = []
        
        i = 0
        while 1 == 1:
            limit.acquire()
            thrd = threading.Thread(target=thr, args=(i,limit))
            threads.append(thrd)
            thrd.start()
            i = i +1
        for t in threads:
                t.join()

    except Exception as e:
        print(e)
        #contador = contador+1
        pass

        
        
    

    


init()
